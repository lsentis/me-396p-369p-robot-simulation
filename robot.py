import math as mth
import numpy as np
from robot_interface import *

# robot interface design
# contains state, update methods, simulation logic
class Robot(RobotInterface):
	# constructor
	def __init__(self, inc_t, grav, dbg):
		self.__dbg = dbg
		self.__grav = grav # private variables
		self.__inc_t = inc_t
		self.__genPos = np.array([[1.12, 0, 0, 0.3, -0.6, 0.3-1.57]]).T
		self.__genVel = np.zeros((6,1))
		self.__genAccel = np.zeros((6,1))
		self.__controlTorque = np.zeros((6,1))
		self.__mass = [0, 0, 10, 2, 2, 1] # transpose
		self.__inertia = [0, 0, 2, 0.3, 0.3, 0.02]
		self.__length = [0, 0, 0.3, 0.45, 0.4, 1.35]
		self.__com = [0, 0, 0, 0.2, 0.2, 0.07]
		self.__M = np.zeros((6,6))
		self.__G  = np.zeros((6,1))
		self.__data = np.zeros((1,7))

	def simulate(self, controlTorque, time):
		self.__updateMath()
		self.__genAccel = np.dot( self.__M, self.__controlTorque ) - self.__G
		self.__genVel += self.__genAccel * self.__inc_t
		self.__genPos += self.__genVel * self.__inc_t + 0.5 * self.__genAccel * self.__inc_t**2

		# debugging
		if self.__dbg:
			print "Mass matrix:\n", self.__M
			print "Gravity vector:\n", self.__G

		# record data
		self.__data = np.append(self.__data, 
			[[time, self.__xf, self.__zf, 
			self.__thetaf, self.__xh, 
			self.__zh, self.__thetah]], 0)

	def getGenPos(self):
		return self.__genPos

	def getGenVel(self):
		return self.__genVel

	def getData(self):
		return self.__data

	def setGenPos(self, value):
		self.__genPos = value

	def setGenVel(self, value):
		self.__genVel = value

	def __updateMath(self):

		# local assignments to compute multibody dynamics
		m3 = self.__mass[2]
		m4 = self.__mass[3]
		m5 = self.__mass[4]
		m6 = self.__mass[5]
		I3 = self.__inertia[2]
		I4 = self.__inertia[3]
		I5 = self.__inertia[4]
		I6 = self.__inertia[5]
		q1 = self.__genPos[0]
		q2 = self.__genPos[1]
		q3 = self.__genPos[2]
		q4 = self.__genPos[3]
		q5 = self.__genPos[4]
		q6 = self.__genPos[5]
		dq1 = self.__genVel[0]
		dq2 = self.__genVel[1]
		dq3 = self.__genVel[2]
		dq4 = self.__genVel[3]
		dq5 = self.__genVel[4]
		dq6 = self.__genVel[5]
		L3 = self.__length[2]
		L4 = self.__length[3]
		L5 = self.__length[4]
		L6 = self.__length[5]
		l3 = self.__com[2]
		l4 = self.__com[3]
		l5 = self.__com[4]
		l6 = self.__com[5]
		s3 = mth.sin( q3 )
		c3 = mth.cos( q3 )
		s34 = mth.sin( q3 + q4 )
		c34 = mth.cos( q3 + q4 )
		s345 = mth.sin( q3 + q4 + q5 )
		c345 = mth.cos( q3 + q4 + q5 )
		s3456 = mth.sin( q3 + q4 + q5 + q6 )
		c3456 = mth.cos( q3 + q4 + q5 + q6 )

		# Jacobian 6
		Jv611 = 0
		Jv612 = 1
		Jv613 = L3*c3 + L4*c34 + L5*c345 + l6*c3456 
		Jv614 = L4*c34 + L5*c345 + l6*c3456
		Jv615 = L5*c345 + l6*c3456
		Jv616 = l6*c3456

		Jv621 = 1
		Jv622 = 0
		Jv623 = L3*s3 + L4*s34 + L5*s345 + l6*s3456
		Jv624 = L4*s34 + L5*s345 + l6*s3456
		Jv625 = L5*s345 + l6*s3456
		Jv626 = l6*s3456

		Jw611 = 0
		Jw612 = 0
		Jw613 = 1
		Jw614 = 1
		Jw615 = 1
		Jw616 = 1

		self.__Jv6 = np.mat([
			[Jv611, Jv612, Jv613, Jv614, Jv615, Jv616],
			[Jv621, Jv622, Jv623, Jv624, Jv625, Jv626]])
		self.__Jw6 = np.mat([
			[Jw611, Jw612, Jw613, Jw614, Jw615, Jw616]])
		self.__J6 = np.concatenate((self.__Jv6, self.__Jw6), axis = 0)

		# Jacobian 5
		Jv511 = 0
		Jv512 = 1
		Jv513 = L3*c3 + L4*c34 + l5*c345
		Jv514 = L4*c34 + l5*c345
		Jv515 = l5*c345
		Jv516 = 0

		Jv521 = 1
		Jv522 = 0
		Jv523 = L3*s3 + L4*s34 + l5*s345
		Jv524 = L4*s34 + l5*s345
		Jv525 = l5*s345
		Jv526 = 0

		Jw511 = 0
		Jw512 = 0
		Jw513 = 1
		Jw514 = 1
		Jw515 = 1
		Jw516 = 0

		self.__Jv5 = np.mat([
			[Jv511, Jv512, Jv513, Jv514, Jv515, Jv516],
			[Jv521, Jv522, Jv523, Jv524, Jv525, Jv526]])
		self.__Jw5 = np.mat([
			[Jw511, Jw512, Jw513, Jw514, Jw515, Jw516]])
		self.__J5 = np.concatenate((self.__Jv5, self.__Jw5), axis = 0)

		# Jacobian 4
		Jv411 = 0
		Jv412 = 1
		Jv413 = L3*c3 + l4*c34
		Jv414 = l4*c34
		Jv415 = 0
		Jv416 = 0

		Jv421 = 1
		Jv422 = 0
		Jv423 = L3*s3 + l4*s34
		Jv424 = l4*s34
		Jv425 = 0
		Jv426 = 0

		Jw411 = 0
		Jw412 = 0
		Jw413 = 1
		Jw414 = 1
		Jw415 = 0
		Jw416 = 0

		self.__Jv4 = np.mat([
			[Jv411, Jv412, Jv413, Jv414, Jv415, Jv416],
			[Jv421, Jv422, Jv423, Jv424, Jv425, Jv426]])
		self.__Jw4 = np.mat([
			[Jw411, Jw412, Jw413, Jw414, Jw415, Jw416]])
		self.__J4 = np.concatenate((self.__Jv4, self.__Jw4), axis = 0)

		# Jacobian 3
		Jv311 = 0
		Jv312 = 1
		Jv313 = 0
		Jv314 = 0
		Jv315 = 0
		Jv316 = 0

		Jv321 = 1
		Jv322 = 0
		Jv323 = 0
		Jv324 = 0
		Jv325 = 0
		Jv326 = 0

		Jw311 = 0
		Jw312 = 0
		Jw313 = 1
		Jw314 = 0
		Jw315 = 0
		Jw316 = 0

		self.__Jv3 = np.mat([
			[Jv311, Jv312, Jv313, Jv314, Jv315, Jv316],
			[Jv321, Jv322, Jv323, Jv324, Jv325, Jv326]])
		self.__Jw3 = np.mat([
			[Jw311, Jw312, Jw313, Jw314, Jw315, Jw316]])
		self.__J3 = np.concatenate((self.__Jv3, self.__Jw3), axis = 0)

		# Jacobian 2
		Jv211 = 0
		Jv212 = 1
		Jv213 = 0
		Jv214 = 0
		Jv215 = 0
		Jv216 = 0

		Jv221 = 1
		Jv222 = 0
		Jv223 = 0
		Jv224 = 0
		Jv225 = 0
		Jv226 = 0

		Jw211 = 0
		Jw212 = 0
		Jw213 = 0
		Jw214 = 0
		Jw215 = 0
		Jw216 = 0

		self.__Jv2 = np.mat([
			[Jv211, Jv212, Jv213, Jv214, Jv215, Jv216],
			[Jv221, Jv222, Jv223, Jv224, Jv225, Jv226]])
		self.__Jw2 = np.mat([
			[Jw211, Jw212, Jw213, Jw214, Jw215, Jw216]])
		self.__J2 = np.concatenate((self.__Jv2, self.__Jw2), axis = 0)

		# Jacobian 1
		Jv111 = 0
		Jv112 = 0
		Jv113 = 0
		Jv114 = 0
		Jv115 = 0
		Jv116 = 0

		Jv121 = 1
		Jv122 = 0
		Jv123 = 0
		Jv124 = 0
		Jv125 = 0
		Jv126 = 0

		Jw111 = 0
		Jw112 = 0
		Jw113 = 0
		Jw114 = 0
		Jw115 = 0
		Jw116 = 0

		self.__Jv1 = np.mat([
			[Jv111, Jv112, Jv113, Jv114, Jv115, Jv116],
			[Jv121, Jv122, Jv123, Jv124, Jv125, Jv126]])
		self.__Jw1 = np.mat([
			[Jw111, Jw112, Jw113, Jw114, Jw115, Jw116]])
		self.__J1 = np.concatenate((self.__Jv1, self.__Jw1), axis = 0)

		# kinematics foot
		self.__xf = q2 + L3*s3 + L4*s34 + L5*s345 + L6/2*s3456
		self.__zf = q1 - L3*c3 - L4*c34 - L5*c345 - L6/2*c3456
		self.__thetaf = q3 + q4 + q5 + q6

		# Jacobian foot
		Jvf11 = 0
		Jvf12 = 1
		Jvf13 = L3*c3 + L4*c34 + L5*c345 + L6/2*c3456 
		Jvf14 = L4*c34 + L5*c345 + L6/2*c3456
		Jvf15 = L5*c345 + L6/2*c3456
		Jvf16 = L6/2*c3456

		Jvf21 = 1
		Jvf22 = 0
		Jvf23 = L3*s3 + L4*s34 + L5*s345 + L6/2*s3456
		Jvf24 = L4*s34 + L5*s345 + L6/2*s3456
		Jvf25 = L5*s345 + L6/2*s3456
		Jvf26 = L6/2*s3456

		Jwf11 = 0
		Jwf12 = 0
		Jwf13 = 1
		Jwf14 = 1
		Jwf15 = 1
		Jwf16 = 1

		self.__Jvf = np.mat([
			[Jvf11, Jvf12, Jvf13, Jvf14, Jvf15, Jvf16],
			[Jvf21, Jvf22, Jvf23, Jvf24, Jvf25, Jvf26]])
		self.__Jwf = np.mat([
			[Jwf11, Jwf12, Jwf13, Jwf14, Jwf15, Jwf16]])
		self.__Jf = np.concatenate((self.__Jvf, self.__Jwf), axis = 0)

		self.__vf = self.__Jvf * self.__genVel
		self.__wf = self.__Jwf * self.__genVel

		# kinematics hill
		self.__xh = q2 + L3*s3 + L4*s34 + L5*s345 - L6/2*s3456
		self.__zh = q1 - L3*c3 - L4*c34 - L5*c345 + L6/2*c3456
		self.__thetah = q3 + q4 + q5 + q6

		# Jacobian hill
		Jvh11 = 0
		Jvh12 = 1
		Jvh13 = L3*c3 + L4*c34 + L5*c345 - L6/2*c3456
		Jvh14 = L4*c34 + L5*c345 - L6/2*c3456
		Jvh15 = L5*c345 - L6/2*c3456
		Jvh16 = - L6/2*c3456

		Jvh21 = 1
		Jvh22 = 0
		Jvh23 = L3*s3 + L4*s34 + L5*s345 - L6/2*s3456
		Jvh24 = L4*s34 + L5*s345 - L6/2*s3456
		Jvh25 = L5*s345 - L6/2*s3456
		Jvh26 = - L6/2*s3456

		Jwh11 = 0
		Jwh12 = 0
		Jwh13 = 1
		Jwh14 = 1
		Jwh15 = 1
		Jwh16 = 1

		self.__Jvh = np.mat([
			[Jvh11, Jvh12, Jvh13, Jvh14, Jvh15, Jvh16],
			[Jvh21, Jvh22, Jvh23, Jvh24, Jvh25, Jvh26]])
		self.__Jwh = np.mat([
			[Jwh11, Jwh12, Jwh13, Jwh14, Jwh15, Jwh16]])
		self.__Jh = np.concatenate((self.__Jvh, self.__Jwh), axis = 0)

		self.__vh = self.__Jvh * self.__genVel
		self.__wh = self.__Jwh * self.__genVel

		# Mass Matrix
		M33 = self.__Jv3.T * m3 * self.__Jv3 + self.__Jw3.T * I3 * self.__Jw3
		M44 = self.__Jv4.T * m4 * self.__Jv4 + self.__Jw4.T * I4 * self.__Jw4
		M55 = self.__Jv5.T * m5 * self.__Jv5 + self.__Jw5.T * I5 * self.__Jw5
		M66 = self.__Jv6.T * m6 * self.__Jv6 + self.__Jw6.T * I6 * self.__Jw6
		self.__M = M33 + M44 + M55 + M66

		# Gravity
		grav_vec = np.array([[0,self.__grav]]).T
		self.__G = ( self.__Jv3.T * m3 + self.__Jv4.T * m4 + 
			self.__Jv5.T * m5 + self.__Jv6.T * m6 ) * grav_vec

		# Actuation matrix
		self.__U = np.array([[0,0,0,1,0,0],[0,0,0,0,1,0],[0,0,0,0,0,1]])


