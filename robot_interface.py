import abc

class RobotInterface:
	
	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def simulate(self, controlTorque):
		pass

	@abc.abstractmethod
	def getGenPos(self):
		pass

	@abc.abstractmethod
	def getGenVel(self):
		pass

	@abc.abstractmethod
	def getData(self):
		pass

	@abc.abstractmethod
	def setGenPos(self, value):
		pass

	@abc.abstractmethod
	def setGenVel(self, value):
		pass
