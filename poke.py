import matplotlib.pyplot as plt
from robot import *
import numpy as np
import sys # to pass command prompt arguments

# debug logic
dbg = False
if len(sys.argv) > 1 and sys.argv[1] == 'debug':
	dbg = True

# physical variables
inc_t = 0.01
grav = 10.

# instantiating the robot object
rr = Robot(inc_t, grav, dbg)

# initialize torques
controlTorque = np.ones((3,1))

# initial values
Q = np.zeros((6,1))
# dQ = np.zeros((6,1))
dQ = np.array([[0,1,1,0,0,0]]).T
rr.setGenPos(Q)
rr.setGenVel(dQ)

def controller(Q, dQ):
	goal = np.zeros((6,1)) # array creation

	# controller gains
	kp = 400.
	kd = 2*kp**0.5 # 40

	# compute controller - PD Control
	return -kp * (Q - goal) - kd * dQ

# time loop
tt = 0
while tt < 1:

	# read the robot state
	Q = rr.getGenPos()
	dQ = rr.getGenVel()

	# simulate robot based on the controller
	# rr.simulate( controller(Q, dQ), tt )

	# simulate robot without a controller
	rr.simulate( np.zeros((6,1)), tt )

	# print rr.getGenPos()

	tt += inc_t

print 'done'

# plotting
data = rr.getData()
legend_size = 8
plt.clf()
plt.subplot(3,1,1)
plt.plot( data[:,0], data[:,1], label = 'xf' )
plt.plot( data[:,0], data[:,4], label = 'xh' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('Positions X')
plt.legend(prop={'size':legend_size})

plt.subplot(3,1,2)
plt.plot( data[:,0], data[:,2], label = 'zf' )
plt.plot( data[:,0], data[:,5], label = 'zh' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('Positions Z')
plt.legend(prop={'size':legend_size})

plt.subplot(3,1,3)
plt.plot( data[:,0], data[:,3], label = 'thetaf' )
plt.plot( data[:,0], data[:,6], label = 'thetah' )
plt.xlabel('t[s]')
plt.ylabel('[rad]')
plt.title('Orientations')
plt.legend(prop={'size':legend_size})

# adjusting subplot
wadjust = 0.4
hadjust = 0.6
plt.subplots_adjust(wspace = wadjust)
plt.subplots_adjust(hspace = hadjust)
plt.savefig('figure.png')

plt.show()
